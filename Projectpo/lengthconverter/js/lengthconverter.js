function cmConverter(){
document.converter.inch.value = document.converter.cm.value / 2.54
document.converter.feet.value = document.converter.cm.value / 30.48
document.converter.yards.value = document.converter.cm.value * 0.010936
document.converter.miles.value = document.converter.cm.value * 0.0000062137
document.converter.meters.value = document.converter.cm.value / 100
}
function inchConverter(){
document.converter.cm.value = document.converter.inch.value / 0.39370
document.converter.feet.value = document.converter.inch.value * 0.083333
document.converter.yards.value = document.converter.inch.value * 0.027778
document.converter.miles.value = document.converter.inch.value * 0.000015783
document.converter.meters.value = document.converter.inch.value / 39.370
}
function feetConverter(){
document.converter.cm.value = document.converter.feet.value / 0.032808
document.converter.inch.value = document.converter.feet.value * 12
document.converter.yards.value = document.converter.feet.value * 0.33333
document.converter.miles.value = document.converter.feet.value * 0.00018939
document.converter.meters.value = document.converter.feet.value / 3.2808
}
function yardsConverter(){
document.converter.cm.value = document.converter.yards.value / 0.010936
document.converter.inch.value = document.converter.yards.value * 36
document.converter.feet.value = document.converter.yards.value * 3
document.converter.miles.value = document.converter.yards.value * 0.00056818
document.converter.meters.value = document.converter.yards.value / 1.0936
}
function milesConverter(){
document.converter.cm.value = document.converter.miles.value / 0.0000062137
document.converter.inch.value = document.converter.miles.value * 63360
document.converter.feet.value = document.converter.miles.value * 5280
document.converter.yards.value = document.converter.miles.value * 1760
document.converter.meters.value = document.converter.miles.value / 0.00062137
}
function metersConverter(){
document.converter.cm.value = document.converter.meters.value / 0.01
document.converter.inch.value = document.converter.meters.value * 39.370
document.converter.feet.value = document.converter.meters.value * 3.2808
document.converter.yards.value = document.converter.meters.value * 1.0936
document.converter.miles.value = document.converter.meters.value * 0.00062137
}